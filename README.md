# FPGArgentina

Relevamiento de grupos de desarrollo, capacidades y casos de uso de la tecnología FPGA en Argentina.
Distribuido bajo licencia *Creative Commons Attribution 4.0 International* [(CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/?ref=ccchooser).

**Disclaimer:** este listado se distribuye con la intención de que resulte útil, pero sin ninguna garantía de exactitud o estado actualizado. Es principalmente resultado de la información recolectada con este [formulario](https://forms.gle/aCuifJgBB6LkRWRh7). Si sos responsable del grupo, y no deseas que el mismo figure en el listado, contactate para que lo removamos.

**Ultima actualización:** 02/09/2020

> Mientras empezamos a procesar el listado para completar la información de cada grupo, seguimos recolectando datos.
> Súmate a través de este [formulario](https://forms.gle/aCuifJgBB6LkRWRh7) (más información en este [artículo](https://www.linkedin.com/pulse/relevamiento-sobre-el-uso-de-la-tecnolog%C3%ADa-fpga-en-argentina-melo/)).
> Si lo completaste, no figura en el listado y no sabes porque, contactanos.

# Directorio

El listado relevado esta segmentado en:
* **Empresas**: deben poseer desarrollo o productos vigentes basado en la tecnología.
* **Organismos/Instituciones**: con grupos de desarrollo activos.
* **Universidades/Facultades**: con grupos o laboratorios de desarrollo activos.
* **Independientes**: profesionales con experiencia, que no estén trabajando dentro de las categorías anteriores, pero ofrezcan servicios activamente.

**Notas:**
* De acuerdo a los objetivos del relevamiento, se descartan cátedras, proyectos finales, cursos, estudiantes o autodidactas.

## Empresas

* Allegro Microsystems <sup>ASIC</sup>
* BlueSphere Technologies
* Boherdi Electrónica
* C7 Technology
* Cognition Business Inteligence
* Emtech SA
* Huenei IT Services
* I-Tera <sup>ASIC</sup>
* Inphi <sup>ASIC</sup>
* INVAP S.E.
* Jotatec
* Microroe Ingeniería Electrónica SRL
* Novo Space
* Satellogic
* Seltron
* Skyloom
* UV-VIS Metrolab S.A.
* VideoSwitch

**Notas:**
* <sup>ASIC</sup> empresa dedicada al desarrollo de ASIC (poseen FPGAs, pero no son el objetivo de sus desarrollos).

## Organismos/Institutos

* Comisión Nacional de Energía Atómica (CNEA) - Instrumentación y Control
* Comisión Nacional de Energía Atómica (CNEA) - Laboratorio Detección de Partículas y Radiación
* Fuerza Aérea Argentina (FAA) - Centro de Investigaciones Aplicadas CIA)
* Fundación Fulgor
* Instituto de Investigaciones Científicas y Técnicas para la Defensa (CITEDEF) - Laboratorio de Técnicas Digitales
* Instituto Nacional de Tecnología Industrial (INTI) - Centro de Micro y Nanotecnologías
* Instituto Nacional de Tecnología Industrial (INTI) - Departamento de Comunicaciones

## Universidades/Facultades

* Facultad de Ingeniería de la Universidad de Buenos Aires (FIUBA) - Laboratorio de Sistemas Embebidos (LSE)
* Universidad Nacional de Córdoba (UNC) - Facultad de Ciencias Exactas, Físicas y Naturales - Departamento de Computación - Laboratorio de Arquitectura de Computadoras
* Universidad Nacional de Córdoba (UNC) - Facultad de Ciencias Exactas, Físicas y Naturales - Departamento de Electrónica - Laboratorio de Comunicaciones Digitales
* Universidad Nacional de la Patagonia Austral (UNPA) - Grupo de Investigación en Optoelectrónica Aplicada (GIOA)
* Universidad Nacional de Mar del Plata (UNMdP) - Instituto de Investigaciones Científicas y Tecnológicas en Electrónica (ICyTE) - Laboratorio de Sistemas Caóticos
* Universidad Nacional de Río Cuarto (UNRC) - Laboratorio de Sistemas Embebidos (LaSEm)
* Universidad Nacional de San Juan (UNSJ) - Laboratorio de Comunicaciones
* Universidad Nacional de San Juan (UNSJ) - Laboratorio de Electrónica Digital
* Universidad Nacional de San Luis (UNSL) - Facultad de Ciencias Físico, Matemáticas y Naturales (FCMyN) - Departamento de Electrónica
* Universidad Nacional del Centro de la Provincia de Buenos Aires (UNICEN) - Facultad de Cs. Exactas - Laboratorio de Sistemas Embebidos Tandil (LabSET)
* Universidad Nacional del Sur (UNS) - Laboratorio de Ingeniería de Computación (LICA) <sup>1</sup>
* Universidad Tecnológica Nacional (UTN) - Facultad Regional Bahía Blanca (FRBB) - Sistemas y Tecnologías de la Información y las Comunicaciones (SITIC) <sup>1</sup>
* Universidad Tecnológica Nacional (UTN) - Facultad Regional Córdoba (FRC) - Centro Universitario de Desarrollo en Automación y Robótica (CUDAR)
* Universidad Tecnológica Nacional (UTN) - Facultad Regional Córdoba (FRC) - Grupo de Investigación y Transferencia en Electrónica Avanzada (GINTEA)
* Universidad Tecnológica Nacional (UTN) - Facultad Regional Haedo (FRH) - Grupo Aplicaciones de Sistemas Embebidos (ASE)

**Notas:**
* <sup>1</sup> Son considerados un solo grupo por parte de su dirección.
